var inputDiem=document.getElementById('diem');
var divErrors= document.getElementById('errors');

function onFormSubmit()
{
    var point=inputDiem.value;
    var res = true;

    if(!IsANumber(point))
    {
        Exception_showErrorInputValue();
        res =  false;
    }
    
    if (res == true && point <= 0 || point >100 )
    {
        Exception_showErrorInputValue();
        res = false;       
    }
    return res;
}

function IsANumber(str){
  return !/\D/.test(str);
}

function Exception_showErrorInputValue()
{
    divErrors.innerHTML =
                '<div>\n' +
                '<p style="font-family: font/SVN-Gilroy; font-weight: bold; color:#ff1b41; font-size: 19px; margin-left: 170px; margin-top: -13px">Số điểm không hợp lệ</p>\n' +
                '</div>'
}