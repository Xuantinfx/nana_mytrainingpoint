
var localStrategy = require('passport-local').Strategy;
var mysql = require('mysql');
var config = require('config');

var database = config.get('database');
var connection = mysql.createConnection(database);

module.exports = function(passport){
   // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user);
    });

    // used to deserialize the user
    passport.deserializeUser(function(user, done) {
        connection.query("SELECT MSSV FROM SINHVIEN WHERE MSSV = '"+user+"';", function(err, rows){
            if(rows.length == 0){ //có thể đây là admin
                connection.query("select TaiKhoan from admin where TaiKhoan = '"+user+"'",function(err2,rows2){
                    if(rows2.length == 0){ //đây không phải admin cũng k phải sinhvien
                        done(null,false);
                    }
                    else{ //đây là admin
                        done(null,user);
                    }
                })
            }
            else //đây là sinh viên
                done(null,user);
        });
    });

    passport.use('local',new localStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },function(req,username,password,done){
            connection.query("select MSSV,MatKhau from SinhVien where MSSV=?",[username],function(err,data){
                if(err){
                    console.log(err);
                    return done(err);
                }
                //Truy vấn k lỗi
                if(data.length == 0) {//có thể ở đây là admin
                    connection.query("select TaiKhoan,MatKhau from admin where TaiKhoan= '"+username+"';",function(err2,data2){
                        if(err2){
                            console.log(err2);
                            done(err2,null);
                        }
                        //truy vấn k có lỗi
                        if(data2.length == 0){ //không phải là admin cũng k phải sv
                            return done(null,false,req.flash('loginMessage','Không tồn tại tài khoản'));
                        }
                        else{ //là admin
                            if(data2[0].MatKhau == password){ //là admin nhập đúng username và password
                                return done(null,username);
                            }
                            else{ //là admin nhưng nhập sai password
                                return done(null,false,req.flash('loginMessage','Sai mật khẩu'));
                            }
                        }
                    })
                }
                else{
                    if(data[0].MatKhau == password){ //là sv nhập đúng username và password
                        return done(null,username);
                    }
                    else{ //là sinh viên nhưng nhập sai password
                        return done(null,false,req.flash('loginMessage','Sai mật khẩu'));
                    }
                }
            })
        }
    ))
}