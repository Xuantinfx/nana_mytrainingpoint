var sinhvien = require('../models/model_sinhvien.js')
var path = require('path');
var fs = require('fs');
var multer = require('multer');

var storage = multer.diskStorage({
    destination:function(req,file,cb){
        cb(null,'./public/img');
    },
    filename: function(req,file,cb){
        cb(null,file.originalname);
    }
 })

var upload = multer({storage:storage});

exports.GetTrangChu = function(req,res){
    sinhvien.ThongTinSinhVien(req,function(err,datasv){
        if(err){
            console.log(err);
            res.render("LoiHeThong");
        }
        else{
            sinhvien.Diem(req,function(err,diem){
                if(err){
                    console.log(err);
                    res.render("LoiHeThong");
                }
                else{
                    res.render('./sinhvien/TrangChu',{datasv:datasv,diem:diem});    
                }
            })
        }
    })
};
exports.GetHoatDong = function(req,res){
    sinhvien.ThongTinSinhVien(req,function(err,datasv){
        if(err){
            console.log(err);
            res.render("LoiHeThong");
        }
        else{
            sinhvien.HoatDong(req,function(err,dshd){
                if(err){
                    console.log(err);
                    res.render('LoiHethong');
                }
                else{
                    sinhvien.Diem(req,function(err,diem){
                        if(err){
                            console.log(err);
                            res.render('LoiHeThong');
                        }
                        else{
                            res.render('./sinhvien/HoatDong',{datasv:datasv,dshd:dshd,diem:diem});                                                    
                        }
                    })
                }
            })
        }
    })
};
exports.GetHoatDongChiTiet = function(req,res){
    sinhvien.ThongTinSinhVien(req,function(err,datasv){
        if(err){
            console.log(err);
            res.render("LoiHeThong");
        }
        else{
            sinhvien.Diem(req,function(err,diem){
                if(err){
                    console.log(err);
                    res.render("LoiHeThong");
                }
                else{
                    let mahd = req.params.id;
                    sinhvien.HoatDongChiTiet(req,mahd,function(err,cthd){
                        if(err){
                            console.log(err);
                            res.render('LoiHeThong');
                        }
                        else{
                            console.log(cthd);
                            res.render('./sinhvien/HoatDongChiTiet',{datasv:datasv,diem:diem,cthd,cthd});             
                        }             
                    })
                }                  
            })
        }
    })
};

exports.GetDangKiHD = function(req,res){
    var mahd = req.params.id;
    var mssv = req.user;
    req.getConnection(function(err,connection){
        var query = connection.query("insert into thamgiahoatdong (MSSV,MaHD,DaThamGia) values('"+mssv+"','"+mahd+"',0)",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{
                res.redirect('/sinhvien/hoatdong')
            }
        })
    })
}

exports.GetHuyDangKi = function(req,res){
    var mahd = req.params.id;
    var mssv = req.user;

    req.getConnection(function(err,connection){
        let query = connection.query("delete from thamgiahoatdong where MSSV='"+mssv+"' and MaHD='"+mahd+"';",function(err){
            if(err){
                res.render('LoiHeThong');
            }
            else{
                res.redirect('/sinhvien/hoatdong');
            }
        })
    })
}

exports.GetThamGia = function(req,res){
    sinhvien.ThongTinSinhVien(req,function(err,datasv){
        if(err){
            console.log(err);
            res.render("LoiHeThong");
        }
        else{
            sinhvien.Diem(req,function(err,diem){
                if(err){
                    console.log(err);
                    res.render('LoiHeThong');
                }
                else{
                    sinhvien.ThamGiaHD(req,function(err,dshd){
                        if(err){
                            console.log(err);
                            res.render('LoiHeThong');
                        }
                        else{
                            var tiendo={I:0,
                                        II:0,
                                        III:0,
                                        IV:0,
                                        V:0,
                                        VI:0};
                            var Tong = [0,0,0,0,0,0];
                            var ThamGia = [0,0,0,0,0,0];
                            //tính toán tiến độ
                            var i=0;
                            for(i = 0;i<dshd.length;i++){
                                Tong[dshd[i].MaLoai - 1]++;
                                if(dshd[i].DaThamGia == 1){
                                    ThamGia[dshd[i].MaLoai - 1]++;
                                }
                            }

                            if(Tong[0] == 0 || ThamGia[0] == 0)
                                tiendo.I = 0;
                            else{
                                tiendo.I = 100*(ThamGia[0]/Tong[0]);    
                            }

                            if(Tong[1] == 0 || ThamGia[1] == 0)
                                tiendo.II = 0;
                            else{
                                tiendo.II = 100*(ThamGia[1]/Tong[1]);    
                            }

                            if(Tong[2] == 0 || ThamGia[2] == 0)
                                tiendo.III = 0;
                            else{
                                tiendo.III = 100*(ThamGia[2]/Tong[2]);    
                            }

                            if(Tong[3] == 0 || ThamGia[3] == 0)
                                tiendo.IV = 0;
                            else{
                                tiendo.IV = 100*(ThamGia[3]/Tong[3]);    
                            }

                            if(Tong[4] == 0 || ThamGia[4] == 0)
                                tiendo.V = 0;
                            else{
                                tiendo.V = 100*(ThamGia[4]/Tong[4]);    
                            }

                            if(Tong[5] == 0 || ThamGia[5] == 0)
                                tiendo.VI = 0;
                            else{
                                tiendo.VI = 100*(ThamGia[5]/Tong[5]);    
                            }

                            res.render('./sinhvien/ThamGia',{datasv:datasv,diem:diem,dshd:dshd,tiendo:tiendo});
                        }
                    }) 
                }
            })
        }
    })
};

exports.GetDaThamGia = function(req,res){
    var mahd = req.params.id;
    var mssv = req.user;
    req.getConnection(function(err,connection){
        connection.query("update thamgiahoatdong set DaThamGia = 1 where MSSV='"+mssv+"' and MaHD='"+mahd+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{
                res.redirect('/sinhvien/thamgia');
            }
        })
    })
}

exports.GetBoThamGia = function(req,res){
    var mahd = req.params.id;
    var mssv = req.user;
    req.getConnection(function(err,connection){
        connection.query("update thamgiahoatdong set DaThamGia = 0 where MSSV='"+mssv+"' and MaHD='"+mahd+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{
                res.redirect('/sinhvien/thamgia');
            }
        })
    })
}

exports.GetGoiY = function(req,res){
    sinhvien.ThongTinSinhVien(req,function(err,datasv){
        if(err){
            console.log(err);
            res.render("LoiHeThong");
        }
        else{
            sinhvien.Diem(req,function(err,diem){
                if(err){
                    console.log(err);
                    res.render('LoiHeThong');
                }
                else{
                    res.render('./sinhvien/GoiY',{datasv:datasv,diem:diem});
                }
            })
        }
    })
};

exports.PostGoiYDiem = function(req,res){
    var diemMuonDat = req.body.diem;
    var dshdTheoDiem = [];
    
    sinhvien.ThongTinSinhVien(req,function(err,datasv){
        if(err){
            console.log(err);
            res.render("LoiHeThong");
        }
        else{
            sinhvien.HoatDong(req,function(err,dshdtemp){
                if(err){
                    console.log(err);
                    res.render('LoiHethong');
                }
                else{
                    sinhvien.Diem(req,function(err,diem){
                        if(err){
                            console.log(err);
                            res.render('LoiHeThong');
                        }
                        else{
                            var tongDiem = 0;
                            var diemThanhPhan = [0,0,0,0,0,0];
                            var dshd = dshdtemp[0];
                            for(var j = 0;j<dshd.length;j++){
                                //console.log(dshd[j]);              
                                if(diemThanhPhan[dshd[j].MaLoai - 1] +dshd[j].SoDRLDatDuoc <= dshd[j].TongDiemToiDa ){
                                    diemThanhPhan[dshd[j].MaLoai - 1] += dshd[j].SoDRLDatDuoc;
                                    tongDiem += dshd[j].SoDRLDatDuoc;
                                    dshdTheoDiem.push(dshd[j]);
                                    //console.log(dshd[j]);
                                    if(tongDiem >= diemMuonDat){
                                        break;
                                    }
                                }
                            }
                            console.log(dshdTheoDiem);
                            res.render('./sinhvien/GoiYDiem',{datasv:datasv,dshd:dshdTheoDiem,diem:diem});                                                    
                        }
                    })
                }
            })
        }
    })
}

exports.GetDangKiHDGoiY = function(req,res){
    var mahd = req.params.id;
    var mssv = req.user;
    req.getConnection(function(err,connection){
        var query = connection.query("insert into thamgiahoatdong (MSSV,MaHD,DaThamGia) values('"+mssv+"','"+mahd+"',0)",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{
                res.redirect('/sinhvien/goiy')
            }
        })
    })
}

exports.GetHuyDangKiHDGoiY = function(req,res){
    var mahd = req.params.id;
    var mssv = req.user;

    req.getConnection(function(err,connection){
        let query = connection.query("delete from thamgiahoatdong where MSSV='"+mssv+"' and MaHD='"+mahd+"';",function(err){
            if(err){
                res.render('LoiHeThong');
            }
            else{
                res.redirect('/sinhvien/goiy');
            }
        })
    })
}

exports.GetLoTrinh = function(req,res){
    sinhvien.ThongTinSinhVien(req,function(err,datasv){
        if(err){
            console.log(err);
            res.redirect('/sinhvien');
        }
        else{
            sinhvien.Diem(req,function(err,diem){
                if(err){
                    console.log(err);
                    res.redirect('/sinhvien');
                }
                else{
                    sinhvien.HoatDongDaDangKi(req,function(err,hoatdongdadangki){
                        if(err){
                            console.log(err);
                            res.redirect('/sinhvien');
                        }
                        else{
                            sinhvien.HoatDongSapToi(req,function(err,dataHDST){
                                if(err){
                                    console.log(err);
                                    res.redirect('/sinhvien');
                                }
                                else{
                                    res.render('./sinhvien/LoTrinh',{datasv:datasv,diem:diem,hoatdongdadangki:hoatdongdadangki,datahdst:dataHDST});                                    
                                }
                            })
                        }                        
                    })
                }              
            })
        }
    })
};

exports.GetXoaHD = function(req,res){
    var mahd = req.params.id;
    var mssv = req.user;

    req.getConnection(function(err,connection){
        let query = connection.query("delete from thamgiahoatdong where MSSV='"+mssv+"' and MaHD='"+mahd+"';",function(err){
            if(err){
                res.render('LoiHeThong');
            }
            else{
                res.redirect('/sinhvien/lotrinh');
            }
        })
    })
}

exports.GetLienHe = function(req,res){
    //lấy danh sách trợ lí sinh viên
    req.getConnection(function(err,connection){
        let query = connection.query("select * from trolisv;",function(err,data){
            if(err){
                console.log(err);
                res.redirect('/sinhvien');
            }
            else{
                let datarender = [];
                //lấy mã khoa sinh viên
                let query2 = connection.query("select MaKhoa from sinhvien where MSSV='"+req.user+"';",function(err,datasv){
                    if(err){
                        console.log(err);
                        res.redirect('/sinhvien');
                    }
                    else{
                        for(var i = 0;i<data.length;i++){
                            if(data[i].MaKhoa == datasv[0].MaKhoa){
                                datarender.push(data[i]);
                            }
                        }
                        sinhvien.ThongTinSinhVien(req,function(err,datasvtemp){
                            if(err){
                                console.log(err);
                                res.redirect('/sinhvien');
                            }
                            else{
                                sinhvien.Diem(req,function(err,diem){
                                    if(err){
                                        console.log(err);
                                        res.redirect('/sinhvien');
                                    }
                                    else{
                                        res.render('./sinhvien/LienHe',{ds_TLSV:datarender,datasv:datasvtemp,diem:diem});    
                                    }                            
                                })
                            }
                        });
                    }
                });
            }
        })
    })
};
exports.GetHelp = function(req,res){
    sinhvien.ThongTinSinhVien(req,function(err,datasv){
        if(err){
            console.log(err);
            res.render("LoiHeThong");
        }
        else{
            sinhvien.Diem(req,function(err,diem){
                res.render('./sinhvien/Help',{datasv:datasv,diem:diem});
            })    
        }
    })
};
exports.GetProfile = function(req,res){
    sinhvien.ThongTinSinhVien(req,function(err,datasv){
        if(err){
            console.log(err);
            res.render("LoiHeThong");
        }
        else{
            sinhvien.ThongTinSinhVienChiTiet(req,function(err,datathongtinchitiet){
                if(err){
                    console.log(err);
                    res.render("LoiHeThong");
                }
                else{
                    sinhvien.Diem(req,function(err,diem){
                        if(err){
                            console.log(err);
                            res.render("LoiHeThong");
                        }
                        else{
                            res.render('./sinhvien/Profile',{datasv:datasv,datathongtinchitiet:datathongtinchitiet,diem:diem});                
                        }
                    })
                }
            })
        }
    })
};
exports.GetDoiMatKhau = function(req,res){
    sinhvien.ThongTinSinhVien(req,function(err,datasv){
        if(err){
            console.log(err);
            res.render("LoiHeThong");
        }
        else{
            sinhvien.Diem(req,function(err,diem){
                if(err){
                    console.log(err);
                    res.render("LoiHeThong");
                }
                else{
                    res.render('./sinhvien/DoiMatKhau',{diem:diem,datasv:datasv,loiMK:undefined});                        
                }
            })
        }
    })
};

exports.PostDoiMatKhau = function(req,res){
    sinhvien.ThongTinSinhVien(req,function(err,datasv){
        if(err){
            console.log(err);
            res.render("LoiHeThong");
        }
        else{
            sinhvien.Diem(req,function(err,diem){
                if(err){
                    console.log(err);
                    res.render("LoiHeThong");
                }
                else{
                    req.getConnection(function(err,connection){
                        var input = req.body;
                        connection.query("select MatKhau from sinhvien where MSSV='"+req.user+"';",function(err,pass){
                            if(input.MKC != pass[0].MatKhau){
                                res.render('./sinhvien/DoiMatKhau',{diem:diem,datasv:datasv,loiMK:5});
                            }
                            if(input.MKC==''||input.MKM1==''||input.MKM2=='')
                            {
                                res.render('./sinhvien/DoiMatKhau',{diem:diem,datasv:datasv,loiMK:1});
                            }
                            else
                            {
                                if(input.MKC==input.MKM2||input.MKC==input.MKM1)
                                {
                                    res.render('./sinhvien/DoiMatKhau',{diem:diem,datasv:datasv,loiMK:2});
                                }
                                else
                                {
                                    if(input.MKM1!=input.MKM2)
                                    {
                                        res.render('./sinhvien/DoiMatKhau',{diem:diem,datasv:datasv,loiMK:3});
                                    }
                                    else
                                    {
                                        var query = connection.query("update SINHVIEN set "+"MatKhau = '"+input.MKM1+"'where MSSV ='"+req.user+"';",function(err,data){
                                            if(err){
                                                console.log(err);
                                                res.render('LoiHeThong');
                                            }
                                            else{
                                                res.render('./sinhvien/DoiMatKhau',{diem:diem,datasv:datasv,loiMK:4});
                                            }
                                        });
                                    }
                                }
                            }
                        })
                        
                    })
                
                }
            })
        }
    })

};

exports.PostTimKiem = function(req,res){
    sinhvien.ThongTinSinhVien(req,function(err,datasv){
        if(err){
            console.log(err);
            res.render("LoiHeThong");
        }
        else{
            var input = req.body;
            sinhvien.TimKiemHoatDong(req,input.KeyWord,function(err,dshd){
                if(err){
                    console.log(err);
                    res.render('LoiHethong');
                }
                else{
                    sinhvien.Diem(req,function(err,diem){
                        if(err){
                            console.log(err);
                            res.render('LoiHeThong');
                        }
                        else{
                            res.render('./sinhvien/TimKiem',{datasv:datasv,dshd:dshd,diem:diem});                                                    
                        }
                    })
                }
            })
        }
    })
}

exports.GetUploadA =  function (req, res) {
    res.render("sinhvien/UpLoadAVT");
}

exports.PostUploadA =  function (req, res) { 
    req.getConnection(function(err,connection){
        connection.query("update sinhvien set CoAvt=1 where MSSV='"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else
            {
                res.redirect('profile');
            }
        })
    }) 
}