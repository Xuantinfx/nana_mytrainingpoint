var express = require('express');
var sinhvien = require('../controller/ctrol_sinhvien');
var path = require('path');
var fs = require('fs');
var multer = require('multer');

var storage = multer.diskStorage({
    destination:function(req,file,cb){
        cb(null,'./public/img');
    },
    filename: function(req,file,cb){
        cb(null,req.user+'.png');
    }
 })

var upload = multer({storage:storage});

var app = express();

app.get('/',sinhvien.GetTrangChu);
app.get('/hoatdong',sinhvien.GetHoatDong);
app.get('/hoatdong/:id',sinhvien.GetHoatDongChiTiet);
app.get('/hoatdong/dangki/:id',sinhvien.GetDangKiHD);
app.get('/hoatdong/huydangki/:id',sinhvien.GetHuyDangKi)
app.get('/thamgia',sinhvien.GetThamGia);
app.get('/thamgia/:id',sinhvien.GetDaThamGia);
app.get('/thamgia/huythamgia/:id',sinhvien.GetBoThamGia);
app.get('/goiy',sinhvien.GetGoiY);
app.post('/goiy/goiydiem',sinhvien.PostGoiYDiem);
app.get('/goiy/goiydiem/dangky/:id',sinhvien.GetDangKiHDGoiY);
app.get('/goiy/goiydiem/huydangky/:id',sinhvien.GetHuyDangKiHDGoiY)
app.get('/lotrinh',sinhvien.GetLoTrinh);
app.get('/lotrinh/xoa/:id',sinhvien.GetXoaHD);
app.get('/lienhe',sinhvien.GetLienHe);
app.get('/help',sinhvien.GetHelp);
app.get('/profile',sinhvien.GetProfile);
app.get('/doimatkhau',sinhvien.GetDoiMatKhau);
app.post('/doimatkhau',sinhvien.PostDoiMatKhau);
app.post('/HoatDongTimKiem',sinhvien.PostTimKiem);
app.get('/uploadavatar',sinhvien.GetUploadA);
app.post('/uploadavatar',upload.single('file'),sinhvien.PostUploadA);
module.exports = app;