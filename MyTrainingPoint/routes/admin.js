var admin = require('../controller/admin');
var express = require('express');

router = express.Router();

router.get('/',admin.GetTrangChu);

router.get('/danh-sach-sv',admin.GetDanhSachSV);
router.get('/danh-sach-sv/them-sv',admin.GetThemSV);
router.post('/danh-sach-sv/them-sv',admin.PostThemSV);
router.get('/danh-sach-sv/sua-sv/:id',admin.GetSuaSV);
router.post('/danh-sach-sv/sua-sv/:id',admin.PostSuaSV);
router.get('/danh-sach-sv/xoa-sv/:id',admin.GetXoaSV);

router.get('/danh-sach-ad',admin.GetDanhSachAD);
router.get('/danh-sach-ad/them-ad',admin.GetThemAD);
router.post('/danh-sach-ad/them-ad',admin.PostThemAD);
router.get('/danh-sach-ad/sua-ad/:id',admin.GetSuaAD);
router.post('/danh-sach-ad/sua-ad/:id',admin.PostSuaAD);
router.get('/danh-sach-ad/xoa-ad/:id',admin.GetXoaAd);

router.get('/danh-sach-hd',admin.GetDanhSachHD);
router.get('/danh-sach-hd/them-hd',admin.GetThemHD);
router.post('/danh-sach-hd/them-hd',admin.PostThemHD);
router.get('/danh-sach-hd/sua-hd/:id',admin.GetSuaHD);
router.post('/danh-sach-hd/sua-hd/:id',admin.PostSuaHD);
router.get('/danh-sach-hd/xoa-hd/:id',admin.GetXoaHD);

router.get('/danh-sach-dvpt',admin.GetDanhSachDVPT);
router.get('/danh-sach-dvpt/them-dvpt',admin.GetThemDVPT);
router.post('/danh-sach-dvpt/them-dvpt',admin.PostThemDVPT);
router.get('/danh-sach-dvpt/sua-dvpt/:id',admin.GetSuaDVPT);
router.post('/danh-sach-dvpt/sua-dvpt/:id',admin.PostSuaDVPT);
router.get('/danh-sach-dvpt/xoa-dvpt/:id',admin.GetXoaDVPT);

router.get('/danh-sach-lhd',admin.GetDanhSachLHD);
router.get('/danh-sach-lhd/them-lhd',admin.GetThemLHD);
router.post('/danh-sach-lhd/them-lhd',admin.PostThemLHD);
router.get('/danh-sach-lhd/sua-lhd/:id',admin.GetSuaLHD);
router.post('/danh-sach-lhd/sua-lhd/:id',admin.PostSuaLHD);
router.get('/danh-sach-lhd/xoa-lhd/:id',admin.GetXoaLHD);

router.get('/danh-sach-khoa',admin.GetDanhSachKHOA);
router.get('/danh-sach-khoa/them-khoa',admin.GetThemKHOA);
router.post('/danh-sach-khoa/them-khoa',admin.PostThemKHOA);
router.get('/danh-sach-khoa/sua-khoa/:id',admin.GetSuaKHOA);
router.post('/danh-sach-khoa/sua-khoa/:id',admin.PostSuaKHOA);
router.get('/danh-sach-khoa/xoa-khoa/:id',admin.GetXoaKHOA);

router.get('/danh-sach-tlsv',admin.GetDanhSachTLSV);
router.get('/danh-sach-tlsv/them-tlsv',admin.GetThemTLSV);
router.post('/danh-sach-tlsv/them-tlsv',admin.PostThemTLSV);
router.get('/danh-sach-tlsv/sua-tlsv/:id',admin.GetSuaTLSV);
router.post('/danh-sach-tlsv/sua-tlsv/:id',admin.PostSuaTLSV);
router.get('/danh-sach-tlsv/xoa-tlsv/:id',admin.GetXoaTLSV);

module.exports = router;