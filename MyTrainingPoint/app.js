var express     = require('express');
var bodyParser  = require('body-Parser');
var path        = require('path');
var session     = require('express-session');
var cookieParser = require('cookie-parser');
var passport    = require('passport');
var flash       = require('connect-flash');
var morgan      = require('morgan');
var admin       = require('./routes/admin');
var sinhvien    = require('./routes/sinhvien');
var taikhoan    = require('./controller/taikhoan');
var mailer      = require('./models/mailer');
var schedule    = require('node-schedule');
var app         = express();

//  Lấy port được cấp hoặc dùng port 8080 nếu hệ thống không cung cấp
var port = process.env.PORT || 8080;
app.listen(port);
console.log('Nana_TrainingPoint listen on port:',port);

//set thư mục mặc định để các file html với đuôi mở rộng là ejs
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//public thư mục public
app.use(express.static(path.join(__dirname, 'public')));

//Sử dụng cho passport
app.use(session({
    secret:"E9873D79C6D87DC0FB6A5778633389",
    saveUninitialized: true,
    resave:true,
    cookie:{
        maxAge: 1000*60*60*24*365
        }
}))
app.use(passport.initialize()); //sử dụng để đăng nhập và duy trì đăng nhập
app.use(passport.session());
app.use(flash()); // use connect-flash for flash messages stored in session

// sử dụng các hàm phục vụ code
app.use(morgan('dev')); // hỗ trợ log vào console để theo dõi
app.use(express.json()); // hỗ trợ encode JSON
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.urlencoded({ extended: true })); // hỗ trợ encode sanng url	

// lấy hàm kết nối csdl
var connection  = require('express-myconnection'); 
var mysql = require('mysql');

// lấy oject dẫn đến csdl
var config = require('config');
var connector = config.get('database');

// set hàm connection thành hàm toàn cục
app.use(connection(mysql,connector,'pool')); //or single

//kiểm tra những hoạt động sắp tới để gửi mail cho sinh viên
//vào 7h tối hằng ngày
schedule.scheduleJob('0 50 8 * * *',mailer.GuiMailNhacSinhVienHDSapToi);

//khởi chạy passport
require('./models/passport')(passport);

app.use('/admin',taikhoan.isAdminLoggedin,admin);
app.use('/sinhvien',taikhoan.isSVLoggedin,sinhvien);

app.get('/',taikhoan.isLoggedIn,function(req,res){
    req.getConnection(function(err,connection){
        connection.query("select TaiKhoan from admin where TaiKhoan='"+req.user+"'",function(err,data){
            if(data.length != 0 && req.isAuthenticated()){
                res.redirect('./admin');
            }
            else{
                res.redirect('/sinhvien');
            }
        })
    })
})
app.get('/login',taikhoan.isNotLoggedIn,function(req,res){
    res.render('login',{message: req.flash('loginMessage')});
});
app.post('/login',passport.authenticate('local',{
    successRedirect : '/sinhvien', // redirect to the secure profile section
    failureRedirect : '/login', // redirect back to the signup page if there is an error
    failureFlash : true // allow flash messages
    })
);
app.get('/logout',function(req,res){
    req.logOut();
    res.redirect('/');
})


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    //next(err);
    res.render('404');
});

module.exports = app;